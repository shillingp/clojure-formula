(ns physics.core)

;(def ^{:units :C} e 1.6e-19)

(def sqr (fn [x] (* x x)))
(def theta (fn [x] (if (nil? x) 1 x)))

(def pi (. Math PI))
(def free-spc 8.85e-12)
(def k 1.38e-23)
(def e 1.60e-19)
(def G 6.67e-11)

(def E {:a (fn [F x] (* (/ 1 2) F x))
        :b (fn [k x] (* (/ 1 2) k (sqr x)))
        :c (fn [F Q] (/ F Q))
        :d (fn [V d] (/ V d))
        :e (fn [Q r] (/ Q (* 4 pi free-spc (sqr r))))
        :f (fn [T] (* (/ 3 2) k T))})
(def F {:a (fn [m a] (* m a))
        :b (fn [k x] (* k x))
        :c (fn [p t] (/ p t))
        :d (fn [m v r] (/ (* m (sqr v)) r))
        :e (fn [M m r] (/ (- (* G M m) (sqr r))))
        :f (fn [Q q r] (/ (* Q q) (* 4 pi free-spc (sqr r))))
        :g (fn [B I L & [t]] (* B I L (theta t)))})

((F :g) 10 10 10)

;((. Math SIN) 0)

